package net.rieksen.networklanguagehologram;

import org.bukkit.plugin.java.JavaPlugin;

import net.rieksen.networkcore.core.NetworkCoreAPI;
import net.rieksen.networkcore.core.importer.JSONImporter;
import net.rieksen.networkcore.core.plugin.INetworkPlugin;

public class NetworkLanguageHologram extends JavaPlugin
{

	public INetworkPlugin getNetworkPlugin()
	{
		return NetworkCoreAPI.getPluginManager().getPlugin(this);
	}

	@Override
	public void onDisable()
	{
		//
	}

	@Override
	public void onEnable()
	{
		this.importDefaults();
		this.initHolograms();
	}

	private void importDefaults()
	{
		// DEBUG
		this.getLogger().info("Importing defaults");
		JSONImporter importer = JSONImporter.loadInputStream(NetworkCoreAPI.getProvider(), this.getResource("defaults.json"));
		importer.importData();
		this.getLogger().info(this.getNetworkPlugin().getMessageSection("Hologram").getMessage("hologram-text").getName());
	}

	private void initHolograms()
	{
		LanguageHologram hologram = new LanguageHologram(this, this.getServer().getWorlds().get(0).getSpawnLocation());
		hologram.init();
	}
}
