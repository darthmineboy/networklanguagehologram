package net.rieksen.networklanguagehologram;

import org.apache.commons.lang.Validate;
import org.bukkit.Location;
import org.bukkit.entity.Player;

import de.inventivegames.hologram.Hologram;
import de.inventivegames.hologram.HologramAPI;
import de.inventivegames.hologram.touch.TouchAction;
import de.inventivegames.hologram.touch.TouchHandler;
import de.inventivegames.hologram.view.ViewHandler;
import net.rieksen.networkcore.core.user.User;

public class LanguageHologram
{

	private NetworkLanguageHologram	plugin;
	private Hologram				hologram;
	private Location				location;

	public LanguageHologram(NetworkLanguageHologram plugin, Location location)
	{
		Validate.notNull(location);
		Validate.notNull(location);
		this.plugin = plugin;
		this.location = location;
	}

	public void init()
	{
		String text = "Loading...";
		this.hologram = HologramAPI.createHologram(this.location, text);
		this.hologram.addViewHandler(new ViewHandler()
		{

			@Override
			public String onView(Hologram hologram, Player player, String message)
			{
				return LanguageHologram.this.plugin.getNetworkPlugin().getMessageSection("Hologram").getMessage("hologram-text")
					.getMessage(User.getUser(player));
			}
		});
		this.hologram.setTouchable(true);
		this.hologram.addTouchHandler(new TouchHandler()
		{

			@Override
			public void onTouch(Hologram hologram, Player player, TouchAction action)
			{
				player.sendMessage("Woow touchy");
			}
		});
		this.hologram.spawn();
		this.hologram.addLineAbove("Line above");
		this.hologram.addLineBelow("Line below");
	}
}
